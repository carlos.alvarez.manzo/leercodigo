import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeectorComponent } from './leector.component';

describe('LeectorComponent', () => {
  let component: LeectorComponent;
  let fixture: ComponentFixture<LeectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeectorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LeectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
