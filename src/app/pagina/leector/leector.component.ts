import { AfterViewInit, Component, OnInit,ViewChild } from '@angular/core';
import { BarcodeScannerLivestreamComponent } from "ngx-barcode-scanner";
import { PeticionesService } from 'src/app/services/peticiones.service';
@Component({
  selector: 'app-leector',
  templateUrl: './leector.component.html',
  styleUrls: ['./leector.component.css']
})
export class LeectorComponent implements AfterViewInit {

  constructor(private dataService: PeticionesService){

  }

  @ViewChild(BarcodeScannerLivestreamComponent)
  barcodeScanner!: BarcodeScannerLivestreamComponent;

  barcodeValue: any;

  ngAfterViewInit() {
    this.barcodeScanner.start();
    let servicio=this.dataService.getCodigoBarra(this.barcodeValue).subscribe();
    console.log(servicio)
  }

  onValueChanges(result: { codeResult: { code: any; }; }) {
    this.barcodeValue = result.codeResult.code;
    let servicio=this.dataService.getCodigoBarra(this.barcodeValue).subscribe();
    console.log(servicio)

   

  }

  onStarted(started: any) {
    console.log(started);
  }

}
