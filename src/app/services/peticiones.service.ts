import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PeticionesService {

  constructor(private http: HttpClient) { }
  

  public getCodigoBarra(Codigo:string){
    return this.http.get<any>('https://www.codigo-alfa.cl/aglo/Login/Codigo');

  }

}
