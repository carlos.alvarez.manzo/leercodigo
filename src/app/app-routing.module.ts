import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeectorComponent } from './pagina/leector/leector.component';

const routes: Routes = [
  { path: '', component: LeectorComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
